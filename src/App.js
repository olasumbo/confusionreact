import "./App.css";
import Main from "./components/mainComponent";
import { Component } from "react";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Main />
      </div>
    );
  }
}

export default App;
