import { Component } from "react";
import { Card, CardBody, CardImg, CardText, CardTitle } from "reactstrap";
import React from "react";

class Dishdetail extends Component {


    renderDish(dish) {
        return (
            <Card>
                <CardImg top src={dish.image} alt={dish.name} />
                <CardBody>
                    <CardTitle>{dish.name}</CardTitle>
                    <CardText>{dish.description}</CardText>
                </CardBody>
            </Card>
        )
    }

    renderComments(comments) {

        if (!comments) {
            return (
                <div></div>
            );
        } else {
            const commentsList = comments.map(comment => {
                return (
                    <li key={comment.id}>
                        <p>{comment.comment}</p>
                        <br />
                        <p>-- {comment.author} , {new Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short', day: '2-digit' }).format(new Date(Date.parse(comment.date)))}</p>
                    </li>

                );
            });
            return (
                <div>
                    <h2>Comments</h2>
                    <ul className="list-unstyled">
                        {commentsList}
                    </ul>
                </div>

            );
        }
    }

    render() {
        if (this.props.dish) {
            return (
                <div className="container">
                    <div className="row">
                        <div className="col-12 col-md-5 m-1">
                            {this.renderDish(this.props.dish)}
                        </div>
                        <div className="col-12 col-md-5 m-1">
                            {this.renderComments(this.props.dish.comments)}
                        </div>
                    </div>
                </div>

            )
        } else {

            return (<div></div>);
        }


    }
}

export default Dishdetail;