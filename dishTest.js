import React, { Component } from "react";
import { Card, CardImg, CardText, CardBody, CardTitle } from "reactstrap";

class DishDetail extends Component {
  renderComments(comments) {
    var dt;
    const comm = comments.map((comment) => {
      dt = new Date(comment.date);

      return (
        <li key={comment.id}>
          <p> {comment.comment}</p>
          <p>
            {" "}
            {comment.author},&nbsp;
            {dt.toLocaleDateString("en-US", {
              year: "numeric",
              month: "long",
              day: "numeric",
            })}
          </p>
        </li>
      );
    });

    return <ul className="list-unstyled">{comm}</ul>;
  }

  renderDish(dish) {
    return (
      <div className="row">
        <div className="col-12 col-md-5 m-1">
          <Card>
            <CardImg top src={dish.image} alt={dish.name} />
            <CardBody>
              <CardTitle>{dish.name}</CardTitle>
              <CardText>{dish.description}</CardText>
            </CardBody>
          </Card>
        </div>
        <div className="col-12 col-md-5 m-1">
          <h4>Comments</h4>
          {this.renderComments(dish.comments)}{" "}
        </div>
      </div>
    );
  }

  render() {
    const dish = this.props.dish;

    if (dish != null) return this.renderDish(dish);
    else return <div></div>;
  }
}

export default DishDetail;
